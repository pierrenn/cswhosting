/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/// The list of tulip addresses was extracted via OCR of https://www.courtlistener.com/recap/gov.uscourts.flsd.521536/gov.uscourts.flsd.521536.512.7.pdf
/// and parsed through the following function (put OCR in file pdf_extract, not included)
fn check_addresses(rpc: &Client) -> Vec<&str> {
    include_str!("../pdf_extract").split('\n').filter(|l| {
        if l.chars().count() < 5 || l.chars().count() > 40 {
            return false;
        }

        let first = l.chars().next().unwrap();
        if first != '1' && first != '3' && first != 'b' { //doubt there is bech32 addresses but who knows xD
            return false;
        }

        if l.find(' ').is_some() {
            return false;
        }

        let address = serde_json::to_value(l.trim_end());
        if !address.is_ok() {
            println!("WARN: skipping line '{}'", l);
            return false;
        }

        let a = rpc.call::<jsonrpc_core::types::params::Params>("validateaddress", &[address.unwrap()]).unwrap();
        if let jsonrpc_core::types::params::Params::Map(m) = a {
            if !m.contains_key("isvalid") {
                println!("WARN: BTC RPC did not returned an isvalid: {:?}", m);
                return false
            }

            let val = m.get("isvalid").unwrap();
            if !val.is_boolean() {
                println!("WARN: BTC RPC did not returned a boolean isvalid: {:?}", m);
                return false
            }

            return val.as_bool().unwrap();
        }

        println!("WARN: BTC RPC not returned a map: {:?}", a);
        false
    }).collect()
}
