/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


use std::process;
use std::env;
use std::collections::BTreeMap;
use std::fs;

use serde_json::json;
use serde::{Serialize};

extern crate bitcoincore_rpc;
use bitcoincore_rpc::{Auth, Client, RpcApi};

#[derive(Default, Debug, Serialize)]
pub struct Signature {
    address: String,
    proof: String,
    is_tulip: bool,
    is_checked: bool,
}

#[derive(Default, Debug, Serialize)]
pub struct FileProof {
    filename: String,
    text: String,
    signatures: Vec<Signature>,
}

#[derive(Default, Debug, Serialize)]
pub struct TulipProof {
    file: usize,   // idx of element of the vector of FileProof containing the signature
    idx:  usize,   // idx of element of the vector of Signature in the FileProof containing the signature
}

#[derive(Default, Debug, Serialize)]
pub struct Tulip {
    amnt:   i64,
    num_tx: i64,
    cb_h:   u64,
    cb_tx:  String,
    cb_ts:  u32,
    last_tx:  String,
    last_ts:  u64,
    proof: Option<TulipProof>,
}


/// Check that a proof for a given address signs a msg
fn check_signature(rpc: &Client, msg: &str, address: &str, proof: &str) -> Result<bool, bitcoincore_rpc::Error> {
    let args = [serde_json::to_value(address)?, serde_json::to_value(proof)?, serde_json::to_value(msg)?];
    rpc.call::<bool>("verifymessage", &args)
}

/// Return the (coinbase txid, coinbase ts) from a block height
fn get_coinbase(rpc: &Client, block_height: u64) -> (String, u32) {
    let hash = rpc.get_block_hash(block_height).expect("could not get block hash from block height");
    let block = rpc.get_block(&hash).expect("could not get block from hash");
    (block.txdata[0].txid().as_hash().to_string(), block.header.time)
}

/// Load the whole tulip set from disk + extra data from BTC RPC node (fetch all addresses on your
/// watchlist, only look at those starting with 'csw')
fn load_tulips(rpc: &Client) -> BTreeMap<String, Tulip> {
    let resp = rpc.list_transactions(Some("*"), Some(1_000_000), Some(0), Some(true))
                  .expect("could not list node transactions");

    let mut ret : BTreeMap<String, Tulip> = BTreeMap::new();
    for tx in resp.into_iter() {
        if !(tx.detail.label.is_some() && tx.detail.label.unwrap().starts_with("csw")) {
            continue;
        }

        let address = tx.detail.address.to_string();
        if ret.contains_key(&address) {
            let elem = ret.get_mut(&address).unwrap();
            (*elem).amnt += tx.detail.amount.as_sat();
            (*elem).num_tx += 1;
            if elem.last_ts < tx.info.time {
                (*elem).last_ts = tx.info.time;
                (*elem).last_tx = tx.info.txid.as_hash().to_string();
            }
        } else {
            ret.insert(address, Default::default());
        }
    }

    let lines : Vec<&str> = include_str!("../data/tulip").split('\n').filter(|l| l.chars().count()>0).collect();
    for l in lines.into_iter() {
        let elem : Vec<&str> = l.trim_end().split(' ').collect();
        assert!(elem.len() == 2, format!("invalid tulip line {}", l));

        println!("'{}' '{}'", elem[0], elem[1]);
        let cb_h = elem[0].parse().expect("invalid block height");
        let (cb_tx, cb_ts) = get_coinbase(rpc, cb_h);

        if !ret.contains_key(elem[1]) {
            ret.insert(String::from(elem[1]), Tulip { cb_tx, cb_ts, cb_h, ..Default::default()});
        } else {
            let t = ret.get_mut(elem[1]).unwrap();
            (*t).cb_h = cb_h;
            (*t).cb_tx = cb_tx;
            (*t).cb_ts = cb_ts;
        }
    }

    ret
}

/// Try to find matching proofs for the tulips
fn load_proofs(rpc: &Client, tulips: &mut BTreeMap<String, Tulip>) -> Vec<FileProof> {
    let paths = fs::read_dir("./data/proofs/").expect("unable to read proof directory");

    paths.map(|f| -> (String, String) {
        let filename = format!("./data/proofs/{}", f.unwrap().file_name().to_str().expect("weird filename"));
        (filename.clone(), fs::read_to_string(filename).expect("could not read").parse().expect("file not valid utf8"))
    }).filter(|(f, c)| -> bool {
        if c.chars().count() < 3 {
            println!("{} too short", f);
            return false;
        }

        if !c.starts_with('"') {
            println!("{} doesn't start with \"", f);
            return false;
        }

        let end_char = c.rfind('"');
        if end_char.unwrap_or(0) == 0 {
            println!("{} couldn't find closing \"", f);
            return false;
        }

        true
    }).enumerate().map(|(file_id, (f, c))| {
        let v : Vec<&str> = c.split('"').collect();
        let text = String::from(v[1]);
        let signatures = v[2].split('\n').filter(|e| !e.starts_with('"') && e.chars().count()>3)
                                         .map(|e| e.trim_end().split(' ').collect::<Vec<&str>>())
                                         .filter(|e| e.len() == 2)
                                         .map(|e| (String::from(e[0]), String::from(e[1])))
                                         .enumerate().map(|(sig_id, (address, proof))| {
            let is_tulip = tulips.contains_key(&address);
            let is_checked = check_signature(&rpc, &text, &address, &proof);
            if is_checked.is_err() {
                println!("WARN: RPC error while checking signature: {:?}, setting it to false.", is_checked);
            }

            let is_checked = is_checked.unwrap_or(false);
            if is_tulip && is_checked {
                let elem = tulips.get_mut(&address).unwrap();
                (*elem).proof = Some(TulipProof { file: file_id, idx: sig_id });
            }

            Signature {
                address,
                proof,
                is_tulip,
                is_checked,
            }
        }).collect();

        FileProof {
            filename: f,
            text,
            signatures,
        }
    }).collect()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 {
        println!("USAGE: {} http://node_address:port user password\nExample: {} http://localhost:8332 me password", args[0], args[0]);
        process::exit(-1);
    }

    let rpc = Client::new(args[1].clone(), Auth::UserPass(args[2].clone(), args[3].clone())).expect("could not create RPC client");
    rpc.get_best_block_hash().expect("Could not connect to the RPC node while attempting test getbestblockhash"); //RPC connection works?

    // Parse input files
    let mut tulips = load_tulips(&rpc);
    let proofs = load_proofs(&rpc, &mut tulips);

    // Dump jsons
    fs::write("data/tulip.out", json!(tulips).to_string()).expect("Unable to write tulip output file");
    fs::write("data/proofs.out", json!(proofs).to_string()).expect("Unable to write proofs output file");
}
