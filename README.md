# areyoucsw

Script + source code of the [areyoucsw.com](https://www.areyoucsw.com)

---

The website is static by design choice (KISS).
If anyone sends a PR who is merged, the website is updated.

# Prerequisites for running the script

You need a rust toolchain and a running btc node.
You can [install `cargo`](https://doc.rust-lang.org/cargo/getting-started/installation.html) if you don't have it already.

If you want to pull extra infos from the RPC node to display, you need to add the 16k addresses to your watch list.
You can do it with a small bash script (will label the addresses `csw{1..16404}`):

```
awk '{ print $2 }' data/tulip > addresses
idx=0;
while IFS='' read -r LINE || [ -n "${LINE}" ]; do
    idx=$((idx+1)); echo $idx $LINE; bitcoin-cli importaddress $LINE csw$idx false
done < addresses
rm -f addresses
```

And relaunch a block scan.


# Usage

```
cargo run http://node_adress:port rpc_user rpc_password
```

# Claiming addresses

The `data/proofs` folder containing all proofs sent to the project.
The format for a proof is:


```
"This is some text which prove I am

someone"

btc_address1 proof1
btc_address2 proof2
```

Hence to add proof, send a mail to the [devel mailing-list](https://lists.sr.ht/~pierrenn/areyoucsw-devel) with a
patch to add a file in the `data/proofs/` folder. Choose any name you want for the filename, as long as it doesn't
clash with others.


Once the mail sent, the script will check if the proofs are valid, the patch will be merged and the site updated.
If you don't know how to send a patch via email, just follow [this simple guide](https://git-send-email.io/).

# License

This project is licensed under the GPLv3 License - see the [COPYING](COPYING) file for details
